# shellcheck shell=sh

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support

# GnuPG
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"              # ~/.gnupg
