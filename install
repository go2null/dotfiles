#!/bin/sh

main() {
	init
	parse_input "$@"
}

init() {
	export PEARL_PKGDIR="$PWD"
	. 'pearl-config/hooks.sh'
	load_env
}

parse_input() {
	case "$1" in
	install)   install; return $?;;
	update)    update ; return $?;;
	uninstall) remove ; return $?;;
	remove)    remove ; return $?;;
	*)
		if [ -z "$1" ]; then
			install
			return $?
		else
			print_fatal "unknown action '$*'"
		fi
		;;
	esac
}

install() {
	shell_startup_hook 'add'
	post_install_hooks
}

update() {
	git checkout main     \
		&& git pull         \
		&& pre_update_hooks \
		&& post_update_hooks
}

remove() {
	pre_remove_hooks
	shell_startup_hook 'remove'
}

# $1 - required. action to take [add|remove]
shell_startup_hook() {
	unset skip

	print_start 'Startup'

	files="$XDG_CONFIG_HOME/bash/bashrc"
	files="$files$NEWLINE$HOME/.bashrc"

	config_line_1="export PEARL_PKGDIR='$PEARL_PKGDIR'"
	config_line_2=". '$PWD/pearl-config/config.sh'"

	ifs="$IFS"
	IFS="$NEWLINE"
	for file in $files; do
		[ ! -e "$file" ] && continue

		file_action="${1}_line_to_file"

		$file_action "$config_line_1" "$file" || skip=1
		$file_action "$config_line_2" "$file" || skip=1
	done
	IFS="$ifs"

	if [ "$skip" = 1 ]; then print_skip; else print_done; fi
}

# == GLOBALS ==

NEWLINE='
'

# == HELPERS ==

# $1 - required. line
# $2 - required. file
add_line_to_file() {
	[ -w "$2" ] || return 1

	grep -q "^[ \t]*$1" "$2" && return 1

	print_line      >> "$2"
	print_line "$1" >> "$2"
}

# $1 - required. line
# $2 - required. file
remove_line_from_file() {
	[ -w "$2" ] || return 1

	sed -i '/^[ \t]*'"$1"'/d' "$2"
}

print_start() (
	len="$(printf '%s' "$*" | wc -m)"
	pad="$((16 - len))"
	[ "$pad" -lt 0 ] && pad=0

 	printf "* %s%${pad}s[" "$*"
)
print_skip()     { printf 'skip'; print_done; }
print_progress() { printf '.'; }
print_done()     { printf ']\n'; }

main "$@"
