# Install

## `pearl` shell package manager

1. Install `pearl` per https://github.com/pearl-core/pearl#installation.
2. Open a new terminal (to get `pearl` settings).
3. Add repo
```sh
[ "$PEARL_HOME" ] || { echo 'Install Pearl first' && exit 1; }
tee --append "${XDG_CONFIG_HOME:=$HOME/.config}/pearl/pearl.conf" <<EOF

PEARL_PACKAGES["dotfiles-go2null"] = {"url": "https://gitlab.com/go2null/dotfiles-go2null.git", "description": "go2null dotfiles"}
EOF
```
4. Install
```sh
pearl install dotfiles-go2null
```

## `install` script

1. Run the included `install` script
```sh
./install        # install
./install update # update
./install remove # remove
```

# Dependencies

## git

1. `install update` uses `git` to update the repo.

## pearl

1. See https://github.com/pearl-core/pearl#dependencies.

## vim

1. `curl` to auto-install [vim-plug](https://github.com/junegunn/vim-plug)
