# shellcheck shell=sh

command_exists 'screen' || return

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support
# Gnu Screen
export SCREENRC="$PEARL_PKGDIR/screen/screenrc"            # ~/.screenrc
