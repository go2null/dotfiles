"# Vim Options

"## "Plug 'vim-airline/vim-airline'"
let g:airline_left_sep                            = ''  " the separator used on the left side >
let g:airline_right_sep                           = ''  " the separator used on the right side >
let g:airline#extensions#tabline#enabled          = 1
let g:airline#extensions#tabline#buffer_nr_show   = 1
let g:airline#extensions#tabline#buffer_nr_format = '%s:'


"# Vim Apps
" "Plug 'wting/gitsessions.vim.git'"
" gitsessions checks gitsessions_dir for OS path separator,
"  (so may not find it on Windows)
let g:VIMFILESDIR            = $XDG_CACHE_HOME . '/vim/'
let g:gitsessions_dir        = $XDG_CACHE_HOME . '/vim/sessions'
let g:gitsessions_use_cache  = 0
nnoremap <Leader>gss :GitSessionSave<CR>
nnoremap <Leader>gsl :GitSessionLoad<CR>

" "Plug 'sjl/gundo.vim'"
nnoremap <Leader>gut :GundoToggle<CR>

" "Plug 'vim-scripts/YankRing.vim'"
" Maintains a history of previous yanks, changes and deletes
let g:yankring_history_dir       = $XDG_CACHE_HOME . '/vim'
let g:yankring_window_use_horiz  = 0
let g:yankring_window_use_right  = 0
let g:yankring_window_width      = 25
let g:yankring_window_auto_close = 0

" "Plug 'airblade/vim-rooter'      " auto change vim currdir to repo root
"  change to file dir if not in a repo
let g:rooter_change_directory_for_non_project_files = 'current'
" don't echo as that requires hitting ENTER to confirm
let g:rooter_silent_chdir = 1


"# Filetypes

"## Go

" "Plug 'fatih/vim-go'"
let g:go_list_type = "quickfix"

"## Markdown

" "Plug 'plasticboy/vim-markdown'"
let g:vim_markdown_folding_level        = 6
let g:vim_markdown_fenced_languages     = ['shell=sh']
let g:vim_markdown_follow_anchor        = 1
let g:vim_markdown_anchorexpr           = "'<<'.v:anchor.'>>'"
let g:vim_markdown_math                 = 1
let g:vim_markdown_frontmatter          = 1
let g:vim_markdown_toml_frontmatter     = 1
let g:vim_markdown_json_frontmatter     = 1
let g:vim_markdown_strikethrough        = 1
let g:vim_markdown_autowrite            = 1
let g:vim_markdown_new_list_item_indent = 0
" " gem install mdl

"## Ruby

" "Plug 'prabirshrestha/vim-lsp'"
"    gem install solargraph
if executable('solargraph')
    " gem install solargraph
    au User lsp_setup call lsp#register_server({
        \ 'name': 'solargraph',
        \ 'cmd': {server_info->[&shell, &shellcmdflag, 'solargraph stdio']},
        \ 'initialization_options': {"diagnostics": "true"},
        \ 'whitelist': ['ruby'],
        \ })
endif

" "Plug ' sunaku/vim-ruby-minitest'"
set completefunc=syntaxcomplete#Complete " i_CTRL-X_CTRL-U completion

"## Shell
" "sudo apt install shellcheck"
" TODO Shell: git clone https://anonscm.debian.org/git/collab-maint/devscripts.git

"## SQL
" "gem install sqlint"

"## YAML
" " cpan; install YAML:XS "


"# Lint, Syntax and Style Checkers
" "Plug 'luochen1990/rainbow'"
let g:rainbow_active = 1           " 0 if you want to enable it later via :RainbowToggle


"# Color schemes
" "Plug 'altercation/vim-colors-solarized'"
let g:solarized_termcolors = 256   " default value is 16
let g:solarized_visibility = 'low' " default value is normal
syntax enable
colorscheme solarized
set background=dark                " has to be after colorscheme

" "Plug 'airblade/vim-gitgutter'"
" Must be after `colorscheme`
"
" Use same default background color as line number column
highlight! link SignColumn LineNr
" Reverse the Sign colors for better visibility with Solarized Dark
highlight GitGutterAdd    ctermfg=Black ctermbg=DarkGreen
highlight GitGutterChange ctermfg=Black ctermbg=DarkYellow
highlight GitGutterDelete ctermfg=Black ctermbg=DarkRed

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=1
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
