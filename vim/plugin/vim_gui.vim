if !has('gui_running') | finish | endif

"# Fonts
if has('gui_gtk3') || has('gui_gtk2')
	let &guifont = "DejaVu\ Sans\ Mono\ 11,
				\\ Liberation\ Mono\ 11,
				\\ Lucida Sans\ Typewriter\ 11"
elseif has('gui_kde')
	let &guifont = "DejaVu\ Sans\ Mono/11/-1/5/50/0/0/0/1/0,
		\\ Liberation\ Mono/11/-1/5/50/0/0/0/1/0,
		\\ Lucida\ Sans\ Typewriter/11/-1/5/50/0/0/0/1/0"
elseif has("gui_macvim")
  let &guifont = "Menlo\ Regular:h14"
elseif has('gui_photon')
	let &guifont = "DejaVu\ Sans\ Mono:s11,
		\\ Liberation\ Mono:s11,
		\\ Lucida\ Sans\ Typewriter:s11"
elseif has('x11')
	let &guifont = '-*-courier-medium-r-normal-*-*-180-*-*-m-*-*'
elseif has ('gui_win64') || has('gui_win32')
	let &guifont = "DejaVu_Sans_Mono:h11:cANSI,
		\Liberation_Mono:h11:cANSI,
		\Lucida_Sans_Typewriter:h11:cANSI"
endif
