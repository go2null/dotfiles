"# New files
if has('multi_byte')
	setglobal fileencoding=utf-8 " default for new files
endif

"# Open - jump to the last position when reopening a file
augroup goto_last_position_on_file_reopen | autocmd!
	autocmd BufReadPost *
		\   if line("'\"") > 1 && line("'\"") <= line("$")
		\ |   exe "normal! g'\""
		\ | endif
augroup END

"# Save - make backup
augroup set_backup_filename | autocmd!
	autocmd BufWritePre * let &backupext = '.'.strftime("%Y%m%dT%H%M%S").'.~'
augroup END
"autocmd BufWritePost * exe "silent !mv ".expand("%:p").&bex.".expand("%:p:h")."/.".expand("%:t").&bex

"# Save - strip whitespace
" http://stackoverflow.com/questions/356126/how-can-you-automatically-remove-trailing-whitespace-in-vim
augroup whitespace_strip_trailing | autocmd!
	autocmd BufWritePre * :%s/\s\+$//e
augroup END

"# Save - with sudo
" http://stackoverflow.com/questions/2600783/how-does-the-vim-write-with-sudo-trick-work
cmap <Leader>w :write !sudo tee > /dev/null %

"# Buffers - allow switching when dirty
" http://stackoverflow.com/questions/2414626/vim-unsaved-buffer-warning-when-switching-files-buffers
set hidden

"# Sessions
set sessionoptions=curdir,folds,help,slash,tabpages,unix,winsize
"set sessionoptions-=options           " allow current vim options to be used
"set sessionoptions-=maps              " allow current vim maps to be used
"set sessionoptions-=buffers           " don't save hidden and unloaded buffers
"set sessionoptions-=sesdir            " don't auto change to dir with session file

"# Mappings - cd/lcd
nnoremap <Leader>cd   :cd %:p:h<CR>:pwd<CR>
nnoremap <Leader>lcd :lcd %:p:h<CR>:pwd<CR>

"# Command-line abbreviations - current path
cabbrev <expr> %%        expand('%:p:h') . '/'
cabbrev <expr> e% 'e ' . expand('%:p:h') . '/'

"# Automatically change local current directory
" Don't use builtin `autochdir` as it changes global PWD
" and thus affects some plugins
" http://stackoverflow.com/a/652632/3366962
augroup auto_set_lcd_to_match_current_buffer | autocmd!
	autocmd BufEnter * silent! lcd %:p:h:gs/ /\\ /
augroup END

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=11
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='

