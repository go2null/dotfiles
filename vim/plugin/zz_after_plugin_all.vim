" Searching
set ignorecase   " ignore case when using a search pattern
set smartcase    " override 'ignorecase' when pattern has upper case characters

" Indenting
filetype indent on

" Use tabs for indentation, spaces for aligning
" http://vim.wikia.com/wiki/Indent_with_tabs,_align_with_spaces#Continuation_lines
set tabstop=2      " Number of spaces a <Tab> in the text stands for
set shiftwidth=2   " Number of spaces used for each step of (auto)indent
set softtabstop=0  " Number of spaces that a <Tab> counts for while performing
" editing operations, like inserting a <Tab> or using <BS>. Make 'tab' and
" 'backspace' insert/delete indents instead of tabs at the beginning of a line
set smarttab       " A <Tab> in an indent (whitespace at start of line) inserts
" 'shiftwidth' spaces. 'tabstop' or 'softtabstop' is used in other places.
set noexpandtab    " Expand <Tab> to spaces in Insert mode
set copyindent     " Copy whitespace for indenting from previous line
set preserveindent " When changing the indent of the current line,
" preserve as much of the indent structure as possible.

