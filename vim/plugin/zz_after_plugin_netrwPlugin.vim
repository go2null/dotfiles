"# Default Key maps
"## The View
" CTRL-l - refresh listing
"      i - cycle listing style: thin, long, wide, tree
"      I - toggle banner
"      s - cycle sort-by name, time, and size
"      r - reverse the current sort
"     qf - display current file info
"     gh - toggle displaying dotfiles
"      a - toggle hiding modes set by regex
"
"## Opening files
"    C - set editing window (target for Open) to current window
" [N]C - set editing window (target for Open) to window with ID `N`
"    p - open file in the Preview window
"    P - open file in the previous window             and move to it
"    t - open file in a new tab                       and move to it
"    o - open file in a new hsplit window to the top  and move to it
"    v - open file in a new vsplit window to the left and move to it
"
"## File management
"   d - create directory
"   % - open new buffer for file in browsed directory
"   D - delete file or (empty) directory
"   R - rename file or directory

"# Options
let g:netrw_dirhistmax   = 100       " default is 10
let g:netrw_home         = $XDG_CONFIG_HOME . '/vim'
let g:netrw_keepdir      = 0         " sync current and browsing directory
let g:netrw_liststyle    = 3         " 0 thin, 1 long, 2 wide, 3 tree
let g:netrw_preview      = 1         " Show preview window in a vertical split
let g:netrw_winsize      = 20        " set default window size [%]
