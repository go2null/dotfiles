"# Clipboards

"## Sync clipboards when swicthing to and from vim
" FocusLost/Gained only work in GUI mode and some terminals
" TODO: doesn't work in MSYS2
augroup sync_vim_and_system_clipboards | autocmd!
	autocmd FocusLost   * let @*=@" | let @+=@"
	autocmd FocusGained * let @"=@* | let @"=@+
augroup END

"## Copy to Clipboard
" use `y` as `<C-c>` doesn't work with `<Leader>`
noremap  <Leader>y     "+yy
vnoremap <Leader>y     "+y

"## Cut and place in Clipboard
vnoremap <Leader><C-x> "+x

"## Paste from Clipboard
noremap  <Leader><C-v> "+p
noremap! <Leader><C-v> <C-r>+


"# Select All
" From $VIMRUNTIME/mswin.vim
noremap  <Leader><C-a>      gggH<C-O>G
inoremap <Leader><C-a> <C-O>gg<C-O>gH<C-O>G
cnoremap <Leader><C-a> <C-C>gggH<C-O>G
onoremap <Leader><C-a> <C-C>gggH<C-O>G
snoremap <Leader><C-a> <C-C>gggH<C-O>G
xnoremap <Leader><C-a> <C-C>ggVG


"# Indentation

"## Renable '>' to indent on Windows
" https://stackoverflow.com/questions/318923/how-to-indent-a-selection-in-gvim-win32
"set selectmode=

"## http://vim.wikia.com/wiki/Fix_indentation
map <Leader>= mzgg=G`z<CR>

"# Folding

set foldmethod=syntax " be language aware
set nofoldenable      " off by default
set foldlevel=2       " default levels to keep open

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=11
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
