"# Auto-install vim-plug

let s:cache_dir  = $XDG_CACHE_HOME  . '/vim'
let s:bundle_dir = s:cache_dir      . '/bundle'

let s:last_update_check = s:bundle_dir . '/last_update_check'
function! s:SetLastUpdateCheck()
	" Check if the directory exists, and create it if it doesn't
	if !isdirectory(s:bundle_dir)
  	call mkdir(s:bundle_dir, 'p')
	endif

	" write status file
	let s:date = strftime('%Y-%m-%d %W')
	call writefile([s:date], s:last_update_check)
endfunction
"
" Place the following code in your .vimrc before plug#begin() call
let s:plug_file = s:cache_dir . '/autoload/plug.vim'
let s:plug_url = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
if empty(glob(s:plug_file))
	silent execute '!curl -fLo ' . s:plug_file . ' --create-dirs ' . s:plug_url
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
	call s:SetLastUpdateCheck()
endif


"# Install plugins
call plug#begin(s:bundle_dir)

"## Vim Options
Plug 'vim-airline/vim-airline'                        " statusline
Plug 'tpope/vim-sensible'                             "
Plug 'tpope/vim-repeat' | Plug 'tpope/vim-unimpaired' " pairs of handy bracket mappings
Plug 'editorconfig/editorconfig-vim'                  " EditorConfig plugin

"## Vim Apps
Plug 'wting/gitsessions.vim'    " Auto save/load vim sessions based on directory and git branch
Plug 'sjl/gundo.vim'            " visualize your Vim undo tree
Plug 'go2null/Mark--Karkat'     " Highlight several words in different colors simultaneously. (#1238 continued)
Plug 'vim-scripts/YankRing.vim' " Maintains a history of previous yanks, changes and deletes
"Plug '~/src/vim-oil'           " Project drawer/explorer
Plug 'tpope/vim-vinegar'        "
Plug 'airblade/vim-rooter'      " auto change vim currdir to repo root
Plug 'majutsushi/tagbar'        " displays tags in a window, ordered by scope
Plug 'godlygeek/tabular'

"## External Apps
Plug 'freitass/todo.txt-vim'  " todo.txt
Plug 'tpope/vim-fugitive'     " Git integration
Plug 'airblade/vim-gitgutter' " Git diff in the gutter
"
"## Filetype Helpers
Plug 'tpope/vim-endwise'    " wisely add `end` in ruby, endfunction/endif/more in vim script, etc
Plug 'tpope/vim-commentary' " un/comment lines and selections

"## Filetypes
Plug 'fatih/vim-go',                                       {'for': 'go'        }
Plug 'pangloss/vim-javascript',                            {'for': 'javascript'}
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'                           , {'for': 'markdown'  }
Plug 'vim-ruby/vim-ruby',                                  {'for': 'ruby'      }
Plug 'sunaku/vim-ruby-minitest',                           {'for': 'ruby'      }
Plug 'tpope/vim-rails',                                    {'for': 'ruby'      }
Plug 'vim-scripts/Super-Shell-Indent',                     {'for': 'sh'        }
Plug 'cespare/vim-toml',                                   {'for': 'toml'      }
Plug 'maralla/vim-toml-enhance',                           {'for': 'toml'      }

"## Lint, Syntax and Style Checkers
Plug 'dense-analysis/ale'      " Linter
Plug 'prabirshrestha/vim-lsp'  " Language Server Client
Plug 'rhysd/vim-lsp-ale'       " Bring vim-lsp into ALE

"## Syntax Highlighting
Plug 'luochen1990/rainbow' " rainbow parentheses improved
Plug 'timcharper/textile.vim'

"## Color schemes
Plug 'altercation/vim-colors-solarized'

call plug#end()


"# Auto-update vim-plug

" Automatically update plugins on weekly basis at startup
if exists("*strftime")
	if strftime('%W') != strftime('%W',getftime(s:last_update_check))
		autocmd VimEnter * PlugUpdate --sync | q | source $MYVIMRC
		call s:SetLastUpdateCheck()
	endif
endif


"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=1
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
