" # Should already be set by general shell startup
" if !exists('$XDG_CONFIG_HOME') || !isdirectory($XDG_CONFIG_HOME)
" 	let $XDG_CONFIG_HOME = $HOME . '/.config'
" endif
"
if !exists('$XDG_CACHE_HOME') || !isdirectory($XDG_CACHE_HOME)
	let $XDG_CACHE_HOME = $HOME . '/.cache'
endif
"
if !exists('$XDG_DATA_HOME') || !isdirectory($XDG_DATA_HOME)
	let $XDG_DATA_HOME = $HOME . '/.local/share'
endif
"
" "# Vim settings
" "## Should already be set by vim shell startup
" let $MYVIMRC="$PEARL_PKGDIR/vim/vimrc"
let s:mydir = expand("<sfile>:p:h")
if !exists('$VIMDOTDIR') || !isdirectory($VIMDOTDIR)
	let $VIMDOTDIR = s:mydir
endif

"## runtimepath
execute "set runtimepath-=" . s:mydir
execute "set runtimepath+=" . s:mydir
execute "set runtimepath-=" . s:mydir . '/after'
execute "set runtimepath+=" . s:mydir . '/after'
" Allow finding VimPlug
set runtimepath-=$XDG_CACHE_HOME/vim
set runtimepath+=$XDG_CACHE_HOME/vim

"## viminfo file
set viminfo+=n$XDG_DATA_HOME/vim/viminfo

"# Vim user dirs
"## backup
set backup
set backupdir=$XDG_CACHE_HOME/vim/backup//
silent call FileMkdir(&backupdir)

"## swapfile
set swapfile
set directory=$XDG_CACHE_HOME/vim/swap//
silent call FileMkdir(&directory)

"## undofile
set undofile
set undodir=$XDG_CACHE_HOME/vim/undo//
silent call FileMkdir(&undodir)

"## viewdir: used by `mkview`
set viewdir=$XDG_DATA_HOME/vim/views//
silent call FileMkdir(&viewdir)

"# Fold this file with ATX style headers - "# is H1, "## is H2, and so on
" Cribbed from https://github.com/godlygeek/vim-files/blob/master/.vimrc
" vim:foldmethod=expr:foldlevel=11
" vim:foldexpr=getline(v\:lnum)=~'^"#'?'>'.(matchend(getline(v\:lnum),'"#*')-1)\:'='
