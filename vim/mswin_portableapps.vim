" = SETUP ENVIRONMENT = "

" gVim started from PortableApps
"	$HOME            = G:\PortableApps\gVimPortable\Data\settings
"	$TEMP            = G:\PortableApps\gVimPortable\Data\Temp
"	$TMP             = G:\PortableApps\gVimPortable\Data\Temp
"	$XDG_RUNTIME_DIR = unset
"
" gVim started from Git-Bash
"	$HOME            = G:\PortableApps\gVimPortable\Data\settings
"	$TEMP            = G:\PortableApps\gVimPortable\Data\Temp
"	$TMP             = G:\PortableApps\gVimPortable\Data\Temp
"	$XDG_RUNTIME_DIR = G:/tmp
"
"	So it seems that gVimPortable is forcing $HOME, $TEMP and $TMP.
"
"	Solution: set values manually in gVimPortable/App/AppInfo/Launcher/gVimPortable.ini
"-HOME=%PAL:DataDir%\settings
"+HOME=%PAL:Drive%\home
"+HOME=G:\home
"+TMP=%HOME%\tmp

if has('win32') || has('win64')
	" allow git to be found, as is used by vim-plug
	let s:git_path = '\..\PortableApps\PortableGit\bin'
	if isdirectory(s:git_path)
		let $PATH .= ';' . $HOME . s:git_path
	endif
endif
