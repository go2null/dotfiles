# shellcheck shell=sh

# This file is sourced whenever a new Bash/Zsh shell is starting up.
#
# Remove this file if bash/zsh configurations are not needed
#
# The following variables can be used:
# - *PEARL_HOME*        - Pearl location
# - *PEARL_PKGNAME*     - Pearl package name
# - *PEARL_PKGREPONAME* - Pearl package repository name
# - *PEARL_PKGDIR*      - Pearl package location
# - *PEARL_PKGVARDIR*   - Pearl package var location

# shellcheck disable=SC1091
. "$PEARL_PKGDIR/0_sh/shrc"
