# shellcheck shell=sh

# = SHELL HARMONIZATION FUNCTIONS = #

# USAGE: command_exists COMMAND
# RETURNS: error level from `command`
#   0     - COMMAND exists as alias, function, or executable
#   other - COMMAND not available
command_exists() {
	command -v "$1" >/dev/null
}

# allow `local`
# Inspired by from https://sh.rustup.rs/rustup-init.sh
#
# It runs on Unix shells like {a,ba,da,k,z}sh. It uses the common `local`
# extension. Note: Most shells limit `local` to 1 var per line, contra bash.
#
# Some versions of ksh have no `local` keyword. Alias it to `typeset`, but
# beware this makes variables global with f()-style function syntax in ksh93.
# mksh has this alias by default.
if ! command_exists 'local' && command_exists 'typeset'; then alias local='typeset'; fi

# use readable `source` instead of easy-to-miss and cryptic `.`
command_exists 'source' || alias source='.'
