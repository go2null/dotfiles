# shellcheck shell=sh

shell_is_login() {
	# check original sourcing filename
	[ "${0##*profile}" = '' ] && return
	[ "${0##*login}"   = '' ] && return

	if [ -n "$BASH" ]; then
		# shellcheck disable=SC3044
		shopt -q login_shell && return || return 1
	fi

	# TODO: don't know how to check other shells, so always return true
	return
}

# http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02
# https://www.gnu.org/software/bash/manual/html_node/Is-this-Shell-Interactive_003f.html#Is-this
shell_is_interactive() {
	# check original sourcing filename
	[ "${0##*shrc}" = '' ] && return

	# shellcheck disable=SC2298
	[ "${$-%i*}" != "$-" ] && return
	[ -n "$PS1"          ] && return

	return 1
}
