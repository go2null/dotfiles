# shellcheck shell=sh disable=SC3043
# `local` is defined in `shell_function_000_posix_enhancements.sh`

# USAGE: path_add [include|prepend|append] 'dir1' 'dir2' ...
#	prepend: add/move to beginning
#	append:  add/move to end
#	include: add to end of PATH if not already included [default]
#          that is, don't change position if already in PATH
# RETURNS:
# prepend:  dir2:dir1:OLD_PATH
# append:   OLD_PATH:dir1:dir2
# If called with no paramters, returns PATH with duplicate directories removed
path_add() {
	local action
	local dir
	local left
	local path
	local right

	case "$1" in
		'include'|'prepend'|'append') action="$1"; shift ;;
		*)                            action='include'   ;;
	esac

	path="$(path_unique "$PATH")"
	path=":$PATH:" # pad to ensure full path is matched later

	for dir in "$@"; do
		[ -d "$dir" ] || continue # skip non-directory params

		case "$action" in
			'include') path="$path$dir:"; continue ;;
			'prepend') path=":$dir$path"; continue ;;
		esac

		left="${path%:"$dir":*}" # remove occurrence to end
		if [ "$path" != "$left" ]; then
			right="${path#*:"$dir":}" # remove start to occurrence
			path="$left:$right"
		fi
		path="$path$dir:"
	done

	# strip ':' pads
	path="${path#:}"
	path="${path%:}"

	# return
	path="$(path_unique "$path")"
	export PATH="$path"
}

path_append()  { path_add 'append'  "$@"; }
path_prepend() { path_add 'prepend' "$@"; }

# USAGE: path_unique [path]
# path - a colon delimited list. Defaults to $PATH is not specified.
# RETURNS: `path` with duplicated directories removed
path_unique() {
	local dir
	local left
	local path_in
	local path_out

	path_in=${1:-$PATH}
	path_out=':'

	# Wrap the while loop in '{}' to be able to access the updated `path variable
	# as the `while` loop is run in a subshell due to the piping to it.
	# https://stackoverflow.com/questions/4667509/shell-variables-set-inside-while-loop-not-visible-outside-of-it
	printf '%s\n' "$path_in" \
		| tr -s ':' '\n' \
		| {
			while IFS='' read -r dir; do
				left="${path_out%:"$dir":*}" # remove last occurrence to end
				if [ "$path_out" = "$left" ]; then
					# PATH doesn't contain $dir
					path_out="$path_out$dir:"
				fi
			done
			# strip ':' pads
			path_out="${path_out#:}"
			path_out="${path_out%:}"
			# return
			printf '%s' "$path_out"
		}
}
