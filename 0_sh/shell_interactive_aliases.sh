# shellcheck shell=sh


# ALIASES AND FUNCTIONS

alias aliases='compgen -A alias'
alias functions_all='compgen -A function'
alias aliases_functions_all='sort -m <(aliases) <(functions_all)'
alias functions='functions_all | grep -vE "^_|_$"'
alias aliases_functions='sort -m <(aliases) <(functions)'
alias alias_defs='alias'
alias function_defs='declare -f | less'
alias alias_function_defs='cat <(function_defs) <(alias)'

# FIND

# find files, ignoring .git dirs and *.sw? files
f() (
	[ -d "$1" ] && { dir="$1"; shift; } || dir="$PWD"

	# default to case insensitive ifilename matching
	if [ $# -eq 1 ] && [ "${1#\-}" = "$1" ]; then
		iname='-iname'
	else
		unset iname
	fi

	find "$dir" \
		-type f \
		-not -path "*/.git/*" \
		-not -path "*/.yardoc/*" \
		-not -path "*/doc/*" \
		-not -name "*~" -not -name "*.sw?" \
		$iname \
		"$@" 2>/dev/null
)
alias fls='f -ls'


# LESS

# https://unix.stackexchange.com/questions/196168/does-less-have-a-feature-like-tail-follow-name-f
alias follow='less -r --follow-name +F'


# GREP

# enable line numbering
grep_opts='-n'
# enable line number alignment if available
{ grep --help 2>&1 | grep -Fqe '--initial-tab'; } && grep_opts="$grep_opts --initial-tab"
# enable color output if available
{ grep --help 2>&1 | grep -Fqe '--color';       } && grep_opts="$grep_opts --color=auto"
# shellcheck disable=SC2139
alias gr="grep $grep_opts"
# case insensitive search
alias gri='gr -i'
# recursive search, excluding common non-code files and diectories
# shellcheck disable=SC2046,SC2086
grf() (
	[ -d "$1" ] && { dir="$1"; shift; } || dir="$PWD"
	# shellcheck disable=SC2046,SC2086
	gr "$@" $(f "$dir");
)
if command -v git >/dev/null; then
	# shellcheck disable=2046 # need the files to be distinct arguements
	grg() { gr "$@" $(git ls-files); }
fi
if grep --help 2>&1 | grep -Fqe '--include'; then
	grep_opts='--recursive'
	grep_opts="$grep_opts"' --exclude="*~"       --exclude="*.swp"'
	grep_opts="$grep_opts"' --exclude-dir=".git" --exclude-dir="coverage"'
	grep_opts="$grep_opts"' --exclude-dir="doc"  --exclude-dir="log"'
	# shellcheck disable=SC2139
	alias grr="gr $grep_opts"
fi


# VIM

# gracefully degrade gvim
if command -v gvim >/dev/null; then
	alias gvi='gvim --remote-silent'
elif command -v vim >/dev/null; then
	if vim -h | grep -Fqe '--remote-silent'; then
		alias gvi='vim --remote-silent'
	else
		alias gvi='vim'
	fi
elif command -v vi >/dev/null; then
	alias gvi='vi'
else
	alias gvi='$EDITOR'
fi


# HISTORY

h() {
	if command_exists history; then
		if [ -n "$1" ]; then
			history | gri "$@"
		else
			history
		fi
	elif [ -r "$HOME/.ash_history" ]; then
		if [ -n "$1" ]; then
			gri "$@" "$HOME/.ash_history"
		else
			cat "$HOME/.ash_history"
		fi
	fi
}


# LS

alias l='ls -Fh --color=auto'
alias la='l -A'
# shellcheck disable=SC2010
if ls --help | grep -Fqe '--time-style'; then
	alias ll='l -l --time-style=+"%Y-%m-%d %T"'
else
	alias ll='l -l'
fi
alias lla='ll -A'


# MISC

alias nanoo='nano -OSiux'

alias path='printf "%s" "$PATH" | tr ":" "\n"'

pstree() { ps -f "$@" | sed 's;\- ;|__;'; }

alias scr='dvtm || screen -Ax || screen -ADRR'

sedr()  { f -print0 | xargs -0 sed -i "s/$1/$2/"; }

# shellcheck disable=SC1004
alias topcpu='echo %CPU %MEM . PID USER ... ARGS; \
ps -eo pcpu,pmem,pid,user,args | sort          -k 1,3nr | head -10'
# shellcheck disable=SC1004
alias topmem='echo %CPU %MEM . PID USER ... ARGS; \
ps -eo pcpu,pmem,pid,user,args | sort -k 2,2nr -k 1,3nr | head -10'

treee() (
	[ -d "$1" ] && { dir="$1"; shift; } || dir="$PWD"
	f "$dir" "$@" \
		| sed \
			-e "s@$dir@@" \
			-e 's@/@|@g' \
			-e 's@^\.|@@' \
			-e 's@[^|][^|]*|@ |@g' \
			-e '/^[. |]*$/d'
)

# http://unix.stackexchange.com/questions/85249/why-not-use-which-what-to-use-then
whichh() { command_exists "$*" && ls -l "$(command -v "$*")" || return 1; }
