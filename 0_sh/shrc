# shellcheck shell=sh

# usage: $1 - script type
_source_shell_startup_scripts() {
	                  _source_scripts "shell_$1"   'sh'
	[ -n "$BASH" ] && _source_scripts "shell_$1" 'bash'
}

# usage:
#	$1 - optional, sort files in descending order ['desc']
#	$2 - script type [filename prefix]
#	$3 - script extension [filename suffix]
#	$4 - optional, subdirectory of $PEARL_PKGDIR. Include leading '/'.
_source_scripts() {
	if [ "$1" = 'desc' ]; then
		desc='-r'
		shift
	else
		unset desc
	fi

	for file in $(find "${PEARL_PKGDIR}${3}" \
		-mindepth 1 -maxdepth 2          \
		-type f -name "${1}*.${2}"       \
		| LC_ALL='C' sort $desc);
	do
		# shellcheck source=/dev/null
		. "$file"
	done
}

_source_shell_startup_scripts 'functions'
if shell_is_interactive; then
	 _source_shell_startup_scripts 'login'
	 _source_shell_startup_scripts 'interactive'
elif shell_is_login; then
 	 _source_shell_startup_scripts 'login'
fi


# == DIRECTORY LAYOUT == #
# .  (root)
# |+ pearl-config/
# |  |- hooks.sh   # sources `APP/install_HOOK.sh` scripts
# |  |- config.sh  # sources `APP/shell_TYPE.SHELL` scripts
# |+ APP/
# |  |- install_post_install.sh
# |  |- install_pre_update.sh
# |  |- install_post_update.sh
# |  |- install_pre_remove.sh
# |  |- shell_functions_NAME.sh
# |  |- shell_functions_NAME.bash
# |  |- shell_login_NAME.sh
# |  |- shell_login_NAME.bash
# |  |- shell_interactive_NAME.sh
# |  |- shell_interactive_NAME.bash


# == shell_*.*sh == #
# shell_login_*.*sh
# - create files and directories
# - set and export environment variables
# - start non-graphical programs that should run at logon (ssh-agent, screen)
# shell_interactive_*.*sh
# - define terminal look-and-feel
#   - set and export terminal-specific environment variables (GREP_COLOR)
#   - set the various prompt strings (PS1/2/4)
# - define functions
# - define aliases


# = SOURCING BY BOURNE SHELLS = #
# http://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html
# https://shreevatsa.wordpress.com/2008/03/30/zshbash-startup-files-loading-order-bashrc-zshrc-etc/
# https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html
#
# bash            # [NON-]INTERACTIVE #     LOGIN #
# 1. /etc/profile
# 2. ~/.bash_profile || ~/.bash_login || ~/.profile
# Z. [ON EXIT] ~/.bash_logout
#
# bash (as `sh`)  # [NON-]INTERACTIVE #     LOGIN #
# 1. /etc/profile
# 2. ~/.profile
#
# bash            #       INTERACTIVE # NON-LOGIN #
#   Includes when invoked by remote shell daemon.
#   (Also includes a new (local) terminal, e.g., xterm.)
# 1. ~/.bashrc
#
# bash            #   NON-INTERACTIVE # NON-LOGIN #
#   E.g., when invoked to run a shell script
# 1. [ -n "$BASH_ENV" ] && . "$BASH_ENV"
#
# bash (as `sh`)  #   NON-INTERACTIVE # NON-LOGIN #
#   E.g., when invoked to run a shell script
# 1. [ -n "$ENV" ] && . "$ENV"


# = RED HAT ENTERPRISE SERVER 6.7 = #
#
# /etc/profile
# -> /etc/profile.d/*.sh
#
# ~/.bash_profile
# -> ~/.bashrc
#    -> /etc/bashrc
#       -> IF login THEN /etc/profile.d/*.sh
#
# ~/.bash_logout [is empty]


# = MSYS2 = #
#
# /etc/bash.bashrc  # bash INTERACTIVE
# -> /etc/profile
#    -> /etc/msystem
#    -> /etc/profile.d/*.sh
#       -> /usr/share/bash-completion/bash_completion
#    -> $MINGW_MOUNT_POINT/etc/profile.d/*.sh
#    -> IF ZSH THEN /etc/profile.d/*.zsh
#    -> IF ZSH THEN $MINGW_MOUNT_POINT/etc/profile.d/*.zsh
#    -> /etc/post-install/*.post
#
# ~/.bash_profile  # bash LOGIN
#   -> ~/.bashrc   # bash INTERACTIVE
#
# ~/.profile       # sh   LOGIN
#   -> ~/.bashrc   # bash INTERACTIVE
#
# /etc/bash.bash_logout # bash LOGIN
#
# ~/.bash_logout

# vim: ft=sh
