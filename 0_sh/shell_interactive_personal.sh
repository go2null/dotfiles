# shellcheck shell=sh disable=SC3043
# `local` is defined in `shell_function_000_posix_enhancements.sh`

# = SHELL - ALIASES & FUNCTIONS - PERSONAL = #

# delete old files and empty directories
# del_old_from Parent-Directory Exclude DaysOld
# $1 - root directory to delete from
# $2 - age, in days
# $* - find criteria
del_old_from() {
	local dir
	local age

	if [ -d "$1" ]; then
		dir="$1"
		shift
	else
		dir="$PWD"
	fi

	[ "$1" -gt 0 ] && age="$2" && shift

	(
		cd "$dir" || exit 1
		find . -type d \( "$@" \) -prune -o -type f -mtime "$age" -exec rm    -v "{}" \; 2>/dev/null
		find . -type d \( "$@" \) -prune -o -type d -empty        -exec rmdir -v "{}" \; 2>/dev/null
	)
}
alias delold-mmc='      del_old_from "/mnt/mmcblk0p1"      "-path ./cache" 180'
alias delold-sdb='      del_old_from "/mnt/sdb1/Downloads" "-path ./cache" 180'
alias delold-downloads='del_old_from "$HOME/Downloads"     "-path ./cache" 180'

alias x-f='xrandr --output eDP1 --auto --primary --output VGA1  --mode 1024x768 --right-of eDP1'
alias x-h='xrandr --output VGA1 --auto --primary --output eDP1  --auto          --right-of VGA1'
alias x-l='xrandr --output eDP1 --auto --primary --output HDMI2 --off                           --output VGA1  --off'
alias x-m='xrandr --output eDP1 --auto --primary --output VGA1  --auto          --right-of eDP1'
alias x-w='xrandr --output eDP1 --auto --primary --output VGA1  --auto          --right-of eDP1 --output HDMI2 --auto --right-of VGA1'
