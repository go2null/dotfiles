# shellcheck shell=sh

# list shell jobs
# http://pubs.opengroup.org/onlinepubs/9699919799/utilities/jobs.html#tag_20_62
j() {
	if [ "$1" = '+' ] || [ "$1" = '%' ]; then # special job_ids - most recent
		fg
	elif [ "$1" = '-' ]; then                # previous to most recent
		%-
	elif [ $(($1-0)) -gt 0 ]; then           # job_id specified
		# shellcheck disable=SC2086
		%$1
	elif [ -n "$1" ]; then                   # (partial) job name specified
		j "$(jobs \
				| grep -iE "^[^ ]+ +[^ ]+ +.*$1" \
				| head -n 1 \
				| sed 's_^\[\([0-9]*\)\].*_\1_')"
	else                                     # list all jobs
		jobs
	fi
}
