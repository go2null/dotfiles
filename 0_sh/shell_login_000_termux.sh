# shellcheck shell=sh

[ -z "$TERMUX_VERSION" ] && return

# ensure is set
export USER="${USER:-$(id -un)}"
