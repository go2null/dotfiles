# = BASH CONFIG = #


## == LAYOUT == ##

# Avoid ugly line wrapping
shopt -s checkwinsize


## == HISTORY == ##

# https://unix.stackexchange.com/questions/18212/bash-history-ignoredups-and-erasedups-setting-conflict-with-common-history

# Allow easy re-edit of multi-line commands
shopt -s cmdhist

# Append to the history instead of overwriting (good for multiple connections)
shopt -s histappend

# Erase duplicate commands;
# and ignore duplicate commands and commands starting with a space
HISTCONTROL=erasedups:ignoreboth

# put history file in cache
mkdir -p "$XDG_CACHE_HOME/bash"
HISTFILE="$XDG_CACHE_HOME/bash/bash_history"

# Append commands to the history file every time a prompt is shown,
# instead of after closing the session.
PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

# keep all history
# https://stackoverflow.com/questions/19454837/bash-histsize-vs-histfilesize
# HISTSIZE=500 # Default 500
HISTFILESIZE=1000000


# = BASH COMPLETION = #

# source system completion scripts
#
# shellcheck source=/dev/null
[ -f '/etc/bash_completion' ] && . '/etc/bash_completion'
#
# shellcheck source=/dev/null
for Y in /etc/bash_completion.d/*.sh                ; do [ -r "$Y" ] && . "$Y"; done
#
# shellcheck source=/dev/null
for Y in "$XDG_CONFIG_HOME"/bash/completion.d/*.bash; do [ -r "$Y" ] && . "$Y"; done
