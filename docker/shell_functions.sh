# shellcheck shell=sh

command_exists 'docker' || return

d() {
	# shellcheck disable=SC2015
	case "$1" in
		'prune') docker_prune ;;
		'i') shift && [ "$1" ] && docker image     "$@" || docker image     ls ;;
		'c') shift && [ "$1" ] && docker container "$@" || docker_container_ls ;;
		'n') shift && [ "$1" ] && docker network   "$@" || docker network   ls ;;
		'v') shift && [ "$1" ] && docker volume    "$@" || docker volume    ls ;;
		*)                        docker           "$@"                        ;;
	esac
}

docker_prune() {
	for object in container volume network image; do
		printf '\n%s\n' "Pruning ${object}s"
		docker $object prune --force
	done
}

docker_container_ls() {
	docker container list \
		--format "table {{.Names}}\t{{.Mounts}}\t{{.Networks}}\t{{.Ports}}\t{{.Status}}"
}
