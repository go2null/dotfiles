[branch]
  autoSetupMerge = true   # imply --track when creating branch
#  autoSetupRebase= never  # rebase when pull

[color]
  branch         = auto   # =auto,false*
  diff           = auto   # =auto,false*
  grep           = auto   # =auto,false*
  interactive    = auto   # =auto,false*
#  pager          = true   # =true*
  showBranch     = auto   # =auto,false*
  status         = auto   # =auto,false*
  ui             = auto   # =auto* since v1.8.4
[color "decorate"]
#  branch         =
#  remoteBranch   =
#  tag            =
#  stash          =
#  HEAD           =
[color "grep"]
#  context        =
#  filename       =
#  function       =
#  linenumber     =
#  match          =
#  matchContext   =
#  matchSelected  =
#  selected       =
#  separator      =
[color "status"]
#  header         =
#  added          =
#  changed        =
#  untracked      =
#  branch         =
#  nobranch       = red
#  unmerged       =

[column]
  ui             = always row nodense

[core]
  # without '--nofork' gvim forks a new process, git sees old process closed,
  # and uses commit text (which wasn't updated)
  #editor        = gvim --nofork --remote-silent
  # not working

  # With 'git log', less displayed color escape sequences instead of rendering color.
  # http://major.io/2013/05/21/handling-terminal-color-escape-sequences-in-less/
  pager          = less -EFginRSwX

  # excludesfile = $XDG_CONFIG_HOME/git/ignore # default # set by post_install hook to this repo
  #
  # whitespace
  whitespace     = trailing-space,space-before-tab

  # end-of-line, checkout per platform, commit as LF, warn if not reversible.
  # use .gitattributes for overrides
  eol            = native # =crlf,lf,native*   # Used on checkout if autocrlf=false.
  autocrlf       = input  # =false*,input,true # Commit as LF. Don't change when checkout.
  safecrlf       = true   # =true*,warn        # Stop if EOL conversion not reversible.

[diff]
  algorithm      = histogram
  mnemonicPrefix = true # true, false*
  renames        = copies
#  submodule      = log

[fetch]
  prune = true

[init]
	defaultBranch = main

[log]

[merge]
  ff             = false # always do a merge commit
  stat           = true  # Always show a diffstat at the end of a merge

[pretty]
  ls             = %C(auto)%ad %h%d %s %C(yellow)[%aN]

[pull]
  ff             = only  # do not create merge commits when pulling

[push]
  # assume --set-upstream when no upstream is set
  autoSetupRemote = true

  # When pushing without giving a refspec,
  # only push the current branch to its upstream tracking branch.
  # This is to prevent accidental pushes to branches
  # which you’re not ready to push yet.
  default        = upstream # =nothing,simple*,upstream,current,matching

[rebase]
  autosquash = true

[status]
#  submodulesummary  = 1  # include status of submodules

[alias]
  # SYNTAX:
  #   2-letter-abbreviation-of-git-command
  #   + CRUD-action: a=add, l=list, u=update, d=delete, UPPER_CASE=force
  #   + r=recursive
  #   + L=last
  #   + u=upstream/remote (vs local)

  newg     = !gitk --all --not ORIG_HEAD & # open gitk with new commit
  dump     = cat-file -p                   # pretty print object
  type     = cat-file -t                   # object type

  ad       = add
  ada      = add --all :/
  adu      = add --update

  br       = branch
  brd      = branch --delete
  brD      = branch --delete --force
  brl      = branch --all --verbose --verbose
  brm      = branch --move
  brM      = branch --move --force

  cf       = config
  cfl      = !git config --list | sort

  cm       = commit
  cma      = commit --verbose -m
  cmu      = commit --amend               # update last commit, providing new message
  cmuL     = commit --amend -C HEAD       # update last commit, reusing same message
  cmf      = commit --fixup               # needs a commit ref, tags commit with fixup!, for later 'rebase --interactive --autosquash'

  co       = checkout
  cob      = checkout -b
  cot      = checkout --track             #  --track implies -b
  coLtg    = !git checkout $( \
                git describe --tags $( \
                  git rev-list --tags --max-count=1)) # checkout last tag

  cp       = cherry-pick

  df       = !git diff                   $1 $2 && git submodule foreach --recursive 'git df'
  dfno     =      diff --name-only
  dfns     = !git diff --name-status -r  $1 $2 && git submodule foreach --recursive 'git dfl'
  dfs      = !git diff --stat        -r  $1 $2 && git submodule foreach --recursive 'git dfs'
  dfst     = diff --staged
  dfw      = !git diff --word-diff       $1 $2 && git submodule foreach --recursive 'git dfw'

  fe       = fetch
  fea      = fetch --all # all remotes

  iw       = instaweb --httpd=webrick         # http://git-scm.com/book/en/Git-on-the-Server-GitWeb
  iws      = instaweb --httpd=webrick --stop

  lg       = lglga -10
  lgl      = log --full-history --pretty=ls --date=short
  lglL     = !sh -c 'git log $1@{1}..$1@{0} "$@"' - # commits created by the last command (e.g., "git pull") # https://git.wiki.kernel.org/index.php/Aliases#What.27s_new.3F
  lgld     = lglg --date-order                      # tree-view of commits, by date-order
  lglda    = lgld --all                             # tree-view of commits, by date-order, for all refs
  lglg     = lgl  --graph                           # tree-view of commits
  lglga    = lglg --all                             # tree-view of commits, for all refs
  lgln     = lgl  --numstat                         # list of commits with files edited and number of add and deletes
  # simplified history showing refs, optionally filtered to specified refs
  lglgas   = "!f() {                              \
                ref_glob="$1";                    \
                [ $# -gt 0 ] && shift;            \
                                                  \
                git lglga "$@"                    \
                  --decorate-refs="*${ref_glob}*" \
                  --simplify-by-decoration        \
                ;                                 \
              }; f"

  mg       = merge

  pl       = pull
  pla      = pull --all --tags --prune

  ps       = push
  psf      = push --force-with-lease
  psbr     = push origin HEAD
  # only push to remotes with `@` in the URL - assumes this mean you have push access
  # and regular `https` clones are not pushed to
  psall    = "!f() {                                                           \
                for R in $(git remote -v | awk '/@.*push/ { print $1 }'); do { \
                  echo "== $R ==";                                             \
                  git push $R $1;                                              \
                }; done                                                        \
              }; f"

  rb       = rebase
  rbi      = rebase --interactive

  rs       = reset
  rsh0     = reset --hard  HEAD
  rsh1     = reset --hard  HEAD^
  rss1     = reset --soft  HEAD^ # http://stackoverflow.com/questions/927358/how-to-undo-the-last-git-commit?rq=1
  rsm0     = reset --mixed HEAD  # unstage all changes

  rt       = remote
  rtl      = !git remote -v && git submodule foreach --recursive 'git remote -v' | sed 's/^Entering.*/= &/'
  rtu      = remote update

  rv       = revert

  sm       = submodule
  smf      = submodule foreach
  smu      = submodule update
  smco     = !git smu --init --checkout --recursive # checkout    superproject commit
  smpl     = !git smu --init --merge    --recursive # merge       superproject commit
  smrb     = !git smu --init --rebase   --recursive # rebase unto superproject commit
  smcou    = !git smcou --remote                    # checkout    upstream commit
  smplu    = !git smplu --remote                    # merge       upstream commit
  smrbu    = !git smrbu --remote                    # rebase unto upstream commit
  smpush   = push --recurse-submodules=on-demand

  st       = status
  str      = !git status && git submodule foreach --recursive 'git status' | sed 's/^Entering.*/= &/'
  sts      = status --short --branch

  tg       = tag

	users    = shortlog --summary --numbered --email # list all authors and committers

  tree     = !git ls-files | sed 's@[^/][^/]*/@ |@g' # tree view

[gui]
  tabsize  = 2

[includeIf "gitdir/i:c:/"]
  path     = config_windows

[includeIf "gitdir/i:/c/"]
  path     = config_windows

[include]
  path     = config_personal

# vim: ft=toml
