# shellcheck shell=sh

! command_exists 'X' && return
#command_exists 'X' || return # errors out in bash docker

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support

# xorg-xauth
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"        # ~/.Xauthority
#TODO: causes Void Linux XFCE to not login

# xorg-xinit
export XINITRC="$PEARL_PKGDIR/X/xinitrc"          # ~/.xinitrc
export XSERVERRC="$PEARL_PKGDIR/X/xserverrc"      # ~/.xserverrc
# Note that these variables are respected by xinit, but not by startx.
