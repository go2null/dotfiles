# shellcheck shell=sh

# https://wiki.archlinux.org/index.php/XDG_Base_Directory_support

# libice
export ICEAUTHORITY="$XDG_RUNTIME_DIR/ICEauthority"    # ~/.ICEauthority
