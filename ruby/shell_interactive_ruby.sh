# shellcheck shell=sh

command_exists 'ruby' || return

# = Ruby= #

# workaround `bundle` binstub hardcoding Gemfile location
b() { BUNDLE_GEMFILE="$(git root)/Gemfile" bundle "$@"; }

alias be='b exec'
alias ber='be rake'

# print file header block
rb_defs() {
	awk '
	{ if ($1=="=begin" && filename != FILENAME)
		{ filename=FILENAME; printf "\033[8;32;44m"filename"\033[0m\n" }
	} /=begin/,/=end/ { print }
	$1=="def"         { printf "\033[8;32;40m"$0"\033[0m\n" }
	' "$@"
}

# print file header block
rb_comment_blocks() {
	if [ -n "$1" ]; then
		files="$*"
	else
		files='*'
	fi
	# TODO: incorporate the `grep` filter into `awk`
	awk '
	{ if ($1=="=begin" && filename != FILENAME)
			{ filename=FILENAME; printf "\033[1;32m"filename"\033[0m\n" }
		} /=begin/,/=end/ { print }
		' $files | grep -v '^='
}
