# shellcheck shell=sh disable=SC3043

print_start 'Ruby'
command_exists 'ruby' || { print_skip; return; }

# $1 - Source
# $2 - Target
_move_dir() {
	local source
	local target

	source="$(readlink "$1" || echo "$1")"
	target="$(readlink "$2" || echo "$2")"

	# at least create target directory
	mkdir -p "$target"

	if [ "$source" = "$target" ]; then :
	elif [ ! -e "$source" ]; then :
	elif [ -f "$source" ]; then cp "$source" "$target/"
	elif [ -d "$source" ]; then cp -r "$source"/* "$target" && rm -rf "$source"
 	else
		print_error "Invalid target directory: '$target'"
		return 1
	fi
}


# GEM

# TODO: verify on Linux. (Not needed on Windows as using RubyInstaller2 and
# it already installs into the user home directory.
# Ideally, would want to set "USER INSTALLATION DIRECTORY" (`user_gemhome`).
#export GEM_HOME="$XDG_DATA_HOME/gem"
#print_progess && mkdir -p "$GEM_HOME"


# BUNDLER

print_progress
_move_dir "$HOME/.bundle/cache"            "$BUNDLE_USER_CACHE/"
_move_dir "$HOME/.bundle/plugin"           "$BUNDLE_USER_PLUGIN/"
_move_dir "$HOME/.bundle"                  "$BUNDLE_USER_HOME"
_move_dir "$XDG_CONFIG_HOME/bundle/cache"  "$BUNDLE_USER_CACHE/"
_move_dir "$XDG_CONFIG_HOME/bundle/plugin" "$BUNDLE_USER_PLUGIN/"
_move_dir "$XDG_CONFIG_HOME/bundle"        "$BUNDLE_USER_HOME"
bundle config set --global bin "$XDG_BIN_HOME"


# PRY

print_progress
mkdir -p "$XDG_CONFIG_HOME/pry"
cp "$PEARL_PKGDIR/ruby/pryrc" "$XDG_CONFIG_HOME/pry/"


# RUBIES

# `ln` doesn't work on Windows without Local Admin or Developer Mode
# Also, using RubyInstaller2 ON windows, so `~/.rubies is not needed/used.

print_progress
[ -z "$MSYSTEM" ] \
	&& _move_dir "$HOME/.rubies" "$XDG_DATA_HOME/rubies" \
	&& ln -s "$XDG_DATA_HOME/rubies" "$HOME/.rubies"


print_done
