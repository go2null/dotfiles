# shellcheck shell=sh

command_exists 'ruby' || return

# = Ruby-on-Rails= #

_rails() {
	# Redmine 3 uses 'bin' instead of 'script'
	# https://github.com/bbatsov/ruby-style-guide#always-warn-at-runtime
	if [ -d 'bin' ]; then
		bundle exec ruby -w    bin/rails "$@"
	else
		bundle exec ruby -w script/rails "$@"
	fi
}
alias irb-='_rails console production'
alias webrick='_rails server webrick -e production'
